#!bash
########################################################################
## Display error and exit
########################################################################
function xerror
{
    cmake -E cmake_echo_color --red --bold "-- $1";
    exit 1;
}

########################################################################
## To fix a bug seen on Mac OS X about the path of R binary
## R is not found correctly -> R-gui is found instead in /Applications/R.app so R CMD ... will not work
## This functions modifies R.cmake to make the code wok under Mac OS X.
########################################################################
function fixMacOSCMake
{
if [ `uname` != "Linux" ]; then
sed -i .bak -e 's/FIND_PROGRAM(R_EXE "R")/IF( ${CMAKE_SYSTEM_NAME} MATCHES "Darwin" )\'$'\n    FIND_PROGRAM(R_EXE "R" PATH_SUFFIXES bin NO_CMAKE_PATH NO_CMAKE_SYSTEM_PATH NO_CMAKE_ENVIRONMENT_PATH )\\\n  ELSE(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")\\\n    FIND_PROGRAM(R_EXE "R")\\\n  ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")/g' "$1"
fi
}

if (( $# < 1 )); then
	xerror "Need Debug or Release";
fi
cmake -E cmake_echo_color --magenta "-- Building $1 Configuration";
 
case $1 in
	"Debug"|"Release") ;;
	*) xerror "Invalid Configuration!";
esac;


YOCTO_ROOT="yocto"
YOCTO_SVN_VER=5573
cmake -E cmake_echo_color --blue --bold "-- Checking yocto sources";

# is source code here ?
if [ -d $YOCTO_ROOT ]; then
	cmake -E cmake_echo_color --black  "-- yocto directory is present";
	(cd $YOCTO_ROOT && svn up -q -r $YOCTO_SVN_VER) || xerror "could't update yocto code!";
else
	cmake -E cmake_echo_color --black  "-- yocto directory not found..";
	cmake -E cmake_echo_color --cyan   "-- fetching code by SVN";
	svn co --trust-server-cert --non-interactive -q https://github.com/ybouret/yocto4/trunk@$YOCTO_SVN_VER $YOCTO_ROOT || xerror "couldn't fetch yocto code!";
fi

#(re)building SDK
cmake -E cmake_echo_color --blue --bold "-- Building yocto SDK...";
fixMacOSCMake yocto/src/share/R.cmake
(cd $YOCTO_ROOT && $SHELL build-main+r.sh gnu $1) || xerror "couldn't build yocto SDK!";

